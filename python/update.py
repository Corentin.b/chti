#!/usr/bin/env python3
'''Mise à jour distante d'une Debian/Ubuntu ou d'une Centos/RHEL'''

import paramiko
from handle_ssh import connect_to_host
import sys

try:
    me,host = sys.argv
    if not host:
        raise ValueError
except ValueError:
    usage = '{} machine\n'.format(sys.argv[0])
    sys.stderr.write(usage)
    exit(1)

c = paramiko.SSHClient()
c.load_system_host_keys()
c.set_missing_host_key_policy(paramiko.AutoAddPolicy())

paramiko.util.log_to_file("paramiko.log", level = "INFO")

print('Mise à jour de {}'.format(host))

connect_to_host(c,host)

stdin, stdout, stderr = c.exec_command('hostname')
print('Hostname :',stdout.read().decode('UTF-8').strip())

stdin, stdout, stderr = c.exec_command("grep '^ *PermitRootLogin *yes' /etc/ssh/sshd_config")
success = ( stdout.channel.recv_exit_status() == 0 )
if success:
    print('AVERTISSEMENT: connection de root ' \
          'en ssh possible avec mot de passe',file=sys.stdout)
    print('Corrigez le fichier /etc/ssh/sshd_config ' \
          'PermitRootLogin prohibit-password',file=sys.stdout)
    exit(127)

stdin, stdout, stderr = c.exec_command('cat /etc/debian_version')
success = ( stdout.channel.recv_exit_status() == 0 )
if success:
    print('Système : Debian')
    is_debian, is_redhat = True, False
    version = stdout.read().decode('UTF-8').strip()

stdin, stdout, stderr = c.exec_command('cat /etc/redhat-release')
success = ( stdout.channel.recv_exit_status() == 0 )
if success:
    print('Système : Red Hat')
    is_debian, is_redhat = False, True 
    version = stdout.read().decode('UTF-8').strip()

print('Version :',version)

if not is_debian and not is_redhat:
    print('DISTRIBUTION INCONNUE !', file=sys.stderr)
    exit(1)

print('Synchronisation avec les dépôts...')
if is_debian:
    stdin,stdout,stderr = c.exec_command('LANG=C apt -y update')
    success = ( stdout.channel.recv_exit_status() == 0 )
else:
    stdin,stdout,stderr = c.exec_command('LANG=C dnf -y check-upgrade')
    success = ( stdout.channel.recv_exit_status() == 0 )

if success:
    print('Synchronisation terminée.')
else:
    print('Échec de la synchronisation.', file=sys.stderr)
    exit(2)

if is_debian:
    stdin,stdout,stderr = c.exec_command('LANG=C apt -y upgrade')
else:
    stdin,stdout,stderr = c.exec_command('LANG=C dnf -y update')

success = ( stdout.channel.recv_exit_status() == 0 )

if success:
    print('Mise à jour terminée.')
else:
    print('Échec de la mise à jour.', file=sys.stderr)
    exit(3)
