#!/usr/bin/env python3
# Proof of Concept: accès ssh par clef privée en Python
# Prérequis : avoir la clef id_rsa.pub qui est de confiance
# sur le compte root de 192.168.105.50
# inspiré de : gist.github.com/batok/2352501

import paramiko

destip = '192.168.105.50'

# La clef privée côté station
# id_rsa.pub correpondant doit être dans le .ssh/authorized_keys
# du serveur destinataire
k = paramiko.RSAKey.from_private_key_file("/home/ib/.ssh/id_rsa")
c = paramiko.SSHClient()
# pas de warning si première connexion
c.set_missing_host_key_policy(paramiko.AutoAddPolicy())

# Go !
c.connect(hostname=destip,username='root')

# on récupère les flux std
stdin, stdout, stderr = c.exec_command('hostname')
# ce sont des fichiers (et des itérateurs)
print(stdout.read().decode('UTF-8').rstrip())

