#!/usr/bin/env python3
'''Module qui encapsule l'exécution à distance.'''

import paramiko
from handle_ssh import connect_to_host

def runremote(host,command,*,verbose=False):
    '''Exécute command sur host.
    Renvoie une séquence décodée en utf-8 et nettoyée :
    stdout, stderr, status'''
    c = paramiko.SSHClient()
    c.load_system_host_keys()
    c.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    paramiko.util.log_to_file("paramiko.log", level = "INFO")

    connect_to_host(c,host)
    stdin, stdout, stderr = c.exec_command(command)
    status = stdout.channel.recv_exit_status()
    results = [ fd.read().decode('UTF-8').strip() for fd in (stdout,stderr) ] + [ status ]
    stdout, stderr = results[:2]
    if verbose:
        print('Commande : {} sur {}'.format(command,host))
        print('STDOUT :',stdout)
        print('STDERR :',stderr)
        print('Status :',status)
    return (stdout, stderr, status)

if __name__ == '__main__':
    serr, sout, stat = runremote('localhost','whoami',verbose=True)
    serr, sout, stat = runremote('localhost','cat /nothere',verbose=True)
    serr, sout, stat = runremote('localhost','hostname',verbose=True)
    serr, sout, stat = runremote('localhost',r'deltree.exe c:\*.*',verbose=True)
